var Directives = new angular.module("Directives", []);

Directives.directive("sideNav", function() {
    return {
        restrict: "E",
        replace: false,
        scope: {
            name: "@name",
            page_name: "@pageName"
        },
        templateUrl: "../views/partials/sidenav.html",
        transclude: true,
        link: function(scope) {
            $("#"+scope.name).prop("disabled", true);
            $("title").html(scope.page_name + " | JS");
        }
    }
});