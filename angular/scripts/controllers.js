
var Controllers = new angular.module("Controllers", []);

var title = " | JS";

Controllers.controller("MainCtrl", ["$scope", "$location", "$interval", "Auth", function($scope, $location, $interval, Auth) {

	setInterval(function() {
		var nonSplash = $(".nonSplashPage");
		if (nonSplash.length) {
			var page_content = $(".page-content");
			var maxHeight = $(window).height();
			maxHeight -= $("#footer").height()*1.5; // Account for footer
			var navbar = $("#nav-collapse-btn-div");
			if (navbar.height() > 0) { // Account for top navbar if active
				maxHeight -= navbar.height();
			}
			page_content.css("max-height", maxHeight); // Set max-height to remaining space
			var resume = $("#resumeContainer");
			if (resume.length) {
				resume.css("height", maxHeight); // Set max-height to remaining space
			}
		}
	}, 25);

	$scope.fetchUser = function() {
		return Auth.fetchUser().then(function(data) {
			$scope.user = data;
			return data;
		});
	};

	$scope.fetchUser();
	$interval(function() {
		$scope.fetchUser();
	}, 1000);
	//TODO: Inherit in child controllers so only loading user in one place

	$scope.logout = function() {
		Auth.logout().then(function() {
			$location.path("/");
		});
	}

}]);

Controllers.controller("SplashCtrl", function() {

	document.title = "Home" + title;

	$(document).ready(function() {
		setInterval(function() {
			var offset = 100;
			var windowHeight = $(window).height();
			var splashContent = $("#splashContent");
			$("#splashHeader").height(windowHeight);
			splashContent.css("margin-top", (windowHeight/2)-(splashContent.height()/2+offset));
		}, 25);
	});

});

Controllers.controller("NavCtrl", ["$scope", "$location", function($scope, $location){

	$scope.click = function(href) {
		$location.path(href);
	}

}]);

Controllers.controller("PortfolioCtrl", ["$scope", function($scope) {

	$scope.items = {
		"nodepress": {
			"id": "nodepress",
			"imgUrl": "../images/nodepress.jpg",
			"title": "NodePress",
			"caption": "My lame attempt to create a CMS using the Node, Angular, Express, and MySQL. Rightfully named NodePress for the inspiration from WordPress.",
			"liveUrl": null,
			"sourceUrl": "https://bitbucket.org/JSchneidler/nodepress/src"
		},
		"parkfinder": {
			"id": "parkfinder",
			"imgUrl": "../images/tf.png",
			"modalImgs": ["../images/tf_wide.png", "../images/tf_profile.png", "../images/tf_place.png"],
			"title": "Trail Finder",
			"caption": "Searches for popular parks and trails near a location using Google's API. Made with Laravel, AngularJS, Bootstrap, and Google's Places and Autocomplete services.",
			"liveUrl": null,
			"sourceUrl": "https://bitbucket.org/JSchneidler/trailfinder/src"
		},
		"twitchstreams": {
			"id": "twitchstreams",
			"imgUrl": "../images/twitch.png",
			"title": "Twitch Streamer's List",
			"caption": "Shows a list of featured and friended streamers and their statuses. Made with AngularJS, jQuery, Bootstrap, and Twitch's API.",
			"liveUrl": "http://codepen.io/Dordan/full/GZYgjL",
			"sourceUrl": "http://codepen.io/Dordan/pen/GZYgjL"
		},
		"wikisearch": {
			"id": "wikisearch",
			"imgUrl": "../images/wikiviewer.png",
			"title": "Wikipedia Search",
			"caption": "Lists results of Wikipedia search. Made with AngularJS, jQuery, Bootstrap, and Wikipedia's API.",
			"liveUrl": "http://codepen.io/Dordan/full/vGzVmK",
			"sourceUrl": "http://codepen.io/Dordan/pen/vGzVmK"
		},
		"google": {
			"id": "google",
			"imgUrl": "../images/google.jpg",
			"title": "Google Place Finder",
			"caption": "Finds any places nearby using Google's API. Predecessor to the newer Trail Finder application. Made with Laravel, AngularJS, Bootstrap, and Google's Places and Autocomplete services.",
			"liveUrl": null,
			"sourceUrl": "https://bitbucket.org/JSchneidler/googleapi/src"
		},
		"pomodoro": {
			"id": "pomodoro",
			"imgUrl": "../images/pomodoro.png",
			"title": "Pomodoro Clock",
			"caption": "Customizable clone of a Pomodoro clock. Made with jQuery and Bootstrap",
			"liveUrl": "http://codepen.io/Dordan/full/GpVvXm",
			"sourceUrl": "http://codepen.io/Dordan/pen/GpVvXm"
		},
		"infinito": {
			"id": "infinito",
			"imgUrl": "../images/infinito.png",
			"title": "Sample Portfolio",
			"caption": "First completed site. Sample single-page portfolio. Made with HTML, CSS, and Bootstrap",
			"liveUrl": "http://codepen.io/Dordan/full/avgYxM",
			"sourceUrl": "http://codepen.io/Dordan/pen/avgYxM"
		}
	};

	$scope.itemClick = function(id) {
		$scope.modalContent = $scope.items[id];
	}

}]);

Controllers.controller("AdminCtrl", ["$scope", "$window", "$location", "Email", "User", "Auth", function($scope, $window, $location, Email, User, Auth) {

	$scope.currentUser = Auth.fetchUser().then(function(user) {
		if (user === null || user.role !== "Admin") {
			$window.location = "/login";
		} else {
			$scope.currentUser = user;
			$scope.emails = Email.GetEmails();
			$scope.users = User.GetUsers();
			$scope.viewing = "main";

			$scope.openEmail = function(id) {
				Email.GetEmail({id: id}).$promise.then(function(email) {
					$scope.email = email;
					$scope.viewing = "email";
				});
			}

			$scope.deleteEmail = function(id) {
				Email.DeleteEmail({id: id}).$promise.then(function() {
					$scope.emails = Email.GetEmails();
					$location.path("admin");
				});
			}

			$scope.openUser = function(id) {
				User.GetUser({id: id}).$promise.then(function(user) {
					$scope.person = user;
					$scope.viewing = "user";
				});
			}

			$scope.deleteUser = function(id) {
				if ($scope.user.id !== id) {
					User.DeleteUser({id: id}).$promise.then(function() {
						$scope.users = User.GetUsers();
						$location.path("admin");
					});
				}
			}

			$scope.close = function() {
				$scope.viewing = "main";
			}
		}
	});

}]);

Controllers.controller("BlogCtrl", ["$scope", function($scope) {

}]);

Controllers.controller("ContactCtrl", ["$scope", "Email", function($scope, Email) {

	$scope.submitted = false;
	$scope.name = "";
	$scope.replyTo = "";
	$scope.subject = "";
	$scope.content = "";

	$scope.submit = function(name, replyTo, subject, content) {
		if (name !== "" && content !== "" && subject !== "") {
			Email.Send({
				name: name,
				replyTo: replyTo,
				subject: subject,
				content: content
			});
			$scope.submitted = true;
		} else {
			//TODO: Handle missing name or content
		}
	}

}]);