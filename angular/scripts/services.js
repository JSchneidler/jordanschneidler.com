var Services = angular.module("Services", ["ngResource"]);

Services.factory("Email", ["$resource",
	function($resource) {
		return $resource("api/emails/:id", {}, {
			Send: { method: "POST" },
			GetEmails: { method: "GET", isArray: true },
			GetEmail: { method: "GET", params: { id: "@id" } },
			DeleteEmail: { method: "DELETE", params: { id: "@id" } }
		});
	}
]);

Services.factory("User", ["$resource",
	function($resource) {
		return $resource("api/users/:id", {}, {
			GetUsers: { method: "GET", isArray: true },
			GetUser: { method: "GET", params: { id: "@id" } },
			DeleteUser: { method: "GET", params: { id: "@id" } }
		});
	}
]);

Services.factory("Auth", ['$http', '$q', function($http, $q) {

	var user;

	function getUser() {
		return user;
	}

	function fetchUser() {
		return $http.get("api/auth").then(function(response) {
			if (response.data.user) {
				user = response.data.user;
			} else {
				user = null;
			}
			return user;
		});
	}

	function logout() {
		return $http.get("api/auth/logout");
	}

	return {
		getUser: getUser,
		fetchUser: fetchUser,
		logout: logout
	};

}]);