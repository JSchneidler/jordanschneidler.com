var app = angular.module("app", [
	"ngRoute",
	"ngResource",
	"Services",
	"Controllers",
	"Directives"
]);


app.config(["$routeProvider", "$locationProvider",
	function($routeProvider, $locationProvider) {
		$routeProvider
			.when("/", {
				templateUrl: "views/splash.html",
				controller: "SplashCtrl"
			})
			.when("/contact", {
				templateUrl: "views/contact.html",
				controller: "ContactCtrl"
			})
			.when("/blog", {
				templateUrl: "views/blog.html",
				controller: "BlogCtrl"
			})
			.when("/login", {
				templateUrl: "views/login.html"
			})
			.when("/register", {
				templateUrl: "views/register.html"
			})
			.when("/admin", {
				templateUrl: "views/admin.html",
				controller: "AdminCtrl"
			})
			.when("/about", {
				templateUrl: "views/about.html"
			})
			.when("/portfolio", {
				templateUrl: "views/portfolio.html",
				controller: "PortfolioCtrl"
			})
			.when("/resume", {
				templateUrl: "views/resume.html"
			})
			.when("/connect", {
				templateUrl: "views/connect.html"
			})
			.when("/devs", {
				templateUrl: "views/devs.html"
			})
			.otherwise({
				templateUrl: "views/404.html"
			});

		$locationProvider.html5Mode(true);

	}
]);
