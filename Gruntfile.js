module.exports = function(grunt) {
    require("time-grunt")(grunt);

    //grunt wrapper function
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        //grunt task config

        clean: {
            public: {
                files: [{
                    expand: true,
                    cwd: "public",
                    src: [
                        "**/*",
                        "!index.php",
                        "!robots.txt",
                        "!web.config",
                        "!.htaccess",
                        "!googlecdb3864fc8c0a98e.html"
                    ]
                }]
            },
            views: {
                files: [{
                    expand: true,
                    cwd: "resources/views",
                    src: "index.php"
                }]
            }
        },

        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            sourceInPlace: {
                files: [{
                    expand: true,
                    cwd: "angular/scripts",
                    src: "**/*.js", // Search folders recursively
                    dest: "angular/scripts"
                }]
            }
        },

        copy: {
            angular: {
                files: [{
                    expand: true,
                    cwd: "angular",
                    dest: "public",
                    src: ["**/*"]
                }]
            },
            angularIndex: {
                files: [{
                    expand: true,
                    cwd: "angular",
                    dest: "public",
                    src: "index.html"
                }]
            },
            angularCopyIndexToLaravel: {
                files: [{
                    expand: true,
                    cwd: "public",
                    src: "index.html",
                    dest: "resources/views",
                    ext: ".php"
                }]
            },
            unminifiedangular: {
                files: [{
                    expand: true,
                    cwd: "angular",
                    dest: "public",
                    src: [
                        "images/**/*",
                        "views/**/*",
                        "favicon.ico"
                    ]
                }]
            },
            components: {
                files: [{
                    expand: true,
                    cwd: "components",
                    src: "**/*",
                    dest: "public/components"
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    cwd: "angular/fonts",
                    src: "**/*",
                    dest: "public/fonts"
                }]
            }
        },

        useminPrepare: {
            html: "angular/index.html",
            options: {
                dest: "public",
                flow: {
                    html: {
                        steps: {
                            js: ["uglifyjs"],
                            css: ["cssmin"]
                        }
                    }
                }
            }
        },

        uglify: { // usemin-created task
            options: {},
            generated: {}
        },

        cssmin: { //usemin-created task
            options: {},
            generated: {}
        },

        usemin: {
            html: "public/index.html",
            options: {
                assetsDirs: "public"
            }
        }

    });

    //Load grunt tasks
    grunt.loadNpmTasks("grunt-usemin");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-ng-annotate");

    //Register grunt build task (for dev)
    grunt.registerTask("build", [
        "clean:public",
        "clean:views",
        "ngAnnotate",
        "copy:angular",
        "copy:angularCopyIndexToLaravel",
        "copy:components",
        "copy:fonts"
    ]);

    //Register grunt build minification task (for prod)
    grunt.registerTask("buildmin", [
        "clean:public",
        "clean:views",
        "ngAnnotate",
        "copy:unminifiedangular",
        "copy:angularIndex",
        "copy:fonts",
        "useminPrepare",
        "uglify:generated",
        "cssmin:generated",
        "usemin",
        "copy:angularCopyIndexToLaravel"
    ]);

    //Cleans public and resources/views folder
    grunt.registerTask("cleanD", [
        "clean:public",
        "clean:views"
    ]);
}