<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User as User;

class UsersController extends ApiController
{
	function __construct() {
		$this->middleware("auth", ["only" => ["update", "destroy"]]);
	}

	public function index() {
		return $this->respondWithData(User::all());
	}

	public function show($id) {
		$user = User::find($id);
		return $this->respondWithData($user);
	}

	public function update($id) {
		//TODO: Update user. PUT /user/{user}
	}

	public function destroy($id) {
		$user = User::find($id);
		return $this->respondWithData($user);
	}

}
