<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Email as Email;

class EmailsController extends ApiController
{
	function __construct() {
		$this->middleware("auth", ["only" => ["update", "destroy"]]);
	}

	public function index() {
		return $this->respondWithData(Email::all());
	}

	public function store(Request $request) {
		$name = ucfirst(strtolower($request->input("name")));
		if ($request->input("replyTo")) {
			$replyTo = $request->input("replyTo");
		} else {
			$replyTo = "None";
		}
		if ($request->input("subject")) {
			$subject = $request->input("subject");
		} else {
			$subject = "None";
		}
		$subject = $request->input("subject");
		$content = $request->input("content");

		Email::create([
			"name" => $name,
			"replyTo" => $replyTo,
			"subject" => $subject,
			"content" => $content
		]);

		$this->sendEmail($name, $replyTo, $subject, $content);
		/*
		return $this->respondWithData([
			"name" => $name,
			"replyTo" => $replyTo,
			"subject" => $subject,
			"content" => $content
		]);
		*/
	}

	public function show($id) {
		$email = Email::find($id);
		if ($email) {
			return $this->respondWithData($email);
		} else {
			return $this->respondNotFound("Email with id not found");
		}
	}

	public function update($id) {
		//TODO: Update email. PUT /email/{email}
	}

	public function destroy($id) {
		$email = Email::find($id);
		if ($email) {
			$email->delete();
		} else {
			$this->respondNotFound("Email with id not found");
		}
	}

	public function sendEmail($name, $replyTo, $subject, $content) {
		Mail::send("emailSubmission", [
			"name" => $name,
			"replyTo" => $replyTo,
			"subject" => $subject,
			"content" => $content,
			"time" => Carbon::now()
		], function($message) {
			$message->to("jordan.schneidler@gmail.com", "Jordan")->subject("Form submission");
		});

		/*
		if (strpos($replyTo, "@") !== false) {
			Mail::send("emailClient", [
				"name" => $name,
				"replyTo" => $replyTo,
				"subject" => $subject,
				"content" => $content
			], function($message) {
				$message->to($replyTo, $name)->subject("Form submission");
			});
		}
		*/
		
	}
}
