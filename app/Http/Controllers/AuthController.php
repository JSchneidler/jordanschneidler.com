<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

class AuthController extends ApiController
{

	public function GetAuth() {
		if (Auth::check()) {
			return $this->respondWithData([
				"user" => Auth::user()
			]);
		}
		return $this->respondWithData("No user");
	}

	public function Logout() {
		Auth::logout();
	}

}
