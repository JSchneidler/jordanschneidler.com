<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $statusCode = 200;

    public function getStatusCode() {
    	return $this->statusCode;
    }

    public function setStatusCode($statusCode) {
    	$this->statusCode = $statusCode;
    	return $this;
    }

    public function respond($data, $headers = []) {
    	return response()->json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithData($data) {
    	return $this->setStatusCode(200)->respond($data);
    }

    public function respondSuccessfulPOST($message = "POST Successful") {
    	return $this->setStatusCode(201)->respond([
    		"message" => $message
    	]);
    }

    public function respondWithError($message = "An error has occurred") {
    	return $this->respond([
    		"error" => $message,
    		"http_status_code" => $this->getStatusCode()
    	]);
    }

    public function respondNotFound($message = "Not Found") {
    	return $this->setStatusCode(404)->respondWithError($message);
    }

    public function respondFailedValidation($message = "Request failed validation") {
    	return $this->setStatusCode(400)->respondWithError($message);
    }

    public function respondFailedAuthentication($message = "Request failed authentication") {
        return $this->setStatusCode(401)->respondWithError($message);
    }

}
