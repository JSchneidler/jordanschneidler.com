<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(["prefix" => "api"], function() {
	Route::resource("emails", "EmailsController");
	Route::resource("users", "UsersController");
	Route::group(["prefix" => "auth"], function() {
		Route::get("/", "AuthController@GetAuth");
		Route::get("/logout", "AuthController@Logout");
	});
});

Route::get("/", "RedirectController@toAngular");

// Authentication Routes...
Route::get('login', 'RedirectController@toAngular'); // Override, let Angular handle login form
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
Route::get('register', 'RedirectController@toAngular'); // Override, let Angular handle registration form
Route::post('register', 'Auth\AuthController@register');

// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm'); //TODO: Override, let Angular handle pwd reset
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');